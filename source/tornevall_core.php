<?php

/**
 * Copyright 2017 Tomas Tornevall & Tornevall Networks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package TorneLIB
 * @version 6.0.0
 */

namespace TorneLIB;

use Exception;

if (file_exists("../vendor/autoload.php")) {require_once( '../vendor/autoload.php' );}

/**
 * Class TorneLIB
 * @package TorneLIB
 * @version 6.0.0
 */
class TorneLIB
{
    /**
     * @package TorneLIB
     * @subpackage TorneLIB-Core
     *
     * The simplifier
     */

    /** @var $CRYPTO MODULE_CRYPTO Encryption and encoding */
    public $CRYPTO;
    /** @var $IO MODULE_IO */
    public $IO;
    /** @var $DATABASE MODULE_DATABASE */
	public $DATABASE;
    /** @var $NETWORK MODULE_NETWORK */
	public $NETWORK;
    /** @var $CURL MODULE_CURL */
	public $CURL;
    /** @var $PLUGIN TorneLIB_Pluggable */
	public $PLUGIN;
    /** @var $SMARTY TorneLIB_Plugin_Smarty */
    private $SMARTY;

	/** @var $version string  */
    private $version = "6.0.0";

    /**
     * TorneLIB constructor. Loads all vital functions
     * @param array $options
     * @throws Exception
     */
    public function __construct($options = array())
    {
        $this->CRYPTO = new MODULE_CRYPTO();
        $this->IO = new MODULE_IO();
        $this->DATABASE = new MODULE_DATABASE();
        $this->NETWORK = new MODULE_NETWORK();
        $this->CURL = new MODULE_CURL();
	    $this->PLUGIN = new TorneLIB_Pluggable();

        /* Initialize API connector */
        //$this->API = new TorneAPI();
    }

    private function initPlugins() {

    }

    public function getCurrentVersion()
    {
        return $this->version;
    }

	/**
	 * @param string $fileName
	 * @param array $assignedVariables
	 * @param bool $isFile
	 *
	 * @return string
	 * @since 6.0.0
	 */
    public function template($fileName = "", $assignedVariables = array(), $isFile = true) {
    	if (!is_object($this->SMARTY)) {
		    $this->SMARTY = new TorneLIB_Plugin_Smarty();
	    }
    	return $this->SMARTY->template($fileName, $assignedVariables, $isFile);
    }

    /**
     * A historically important function for most of the web projects running from TornevallWEB. Will still be available from core
     *
     * @param string $fileName
     * @param array $assignedVariables
     * @param bool $isFile
     * @return mixed
     * @throws Exception
     * @deprecated 6.0.0
     */
    public function evaltemplate($fileName = "", $assignedVariables = array(), $isFile = true) {
	    return $this->template( $fileName, $assignedVariables, $isFile );
    }

	/**
	 * Convert object to a data object (used for repairing __PHP_Incomplete_Class objects)
	 *
	 * @param array $objectArray
	 * @param bool $useJsonFunction
	 *
	 * @return object
	 * @since 5.0.0
	 */
	function arrayObjectToStdClass($objectArray = array(), $useJsonFunction = false) {
    	return $this->IO->arrayObjectToStdClass($objectArray, $useJsonFunction);
	}

	/**
	 * Convert objects to arrays
	 *
	 * @param $arrObjData
	 *
	 * @return mixed
	 * @since 5.0.0
	 */
    public function objectsIntoArray($arrObjData)
    {
    	return $this->IO->objectsIntoArray($arrObjData);
    }

    /**
     * base64_encode for urls
     *
     * @param $data
     * @return string
     * @since 5.0.0
     */
    public function base64url_encode($data)
    {
    	$this->CRYPTO->base64url_encode($data);
    }

    /**
     * base64_decode for urls
     * @param $data
     * @return string
     * @since 5.0.0
     */
    public function base64url_decode($data)
    {
    	$this->CRYPTO->base64url_decode($data);
    }

    /**
     * Finds out if a bitmasked value is located in a bitarray
     *
     * @param int $requestedBitValue
     * @param int $requestedBitSum
     *
     * @return bool
     * @since 5.0.0
     */
    public function isBit($requestedBitValue = 0, $requestedBitSum = 0)
    {
    	$this->NETWORK->BIT->isBit($requestedBitValue, $requestedBitSum);
    }

    public function __call( $name, $arguments ) {
    	// TODO: Implement class-vs-method-fetcher
    }
}
