<?php

use TorneLIB\TorneLIB;
use PHPUnit\Framework\TestCase;

if (file_exists("../vendor/autoload.php")) {require_once( '../vendor/autoload.php' );}

class TorneLIBTest extends TestCase {

	/** @var $LIB TorneLIB */
	private $LIB;

	private $arr = array(
		'a' => 'b',
		'b' => array(
			'c' => 'd'
		)
	);
	private $obj;

	function setUp() {
		$this->obj               = new stdClass();
		$this->obj->a            = new stdClass();
		$this->obj->a->nextLevel = array(
			'arrayLevel' => "part 1",
			'nextLevel'  => array(
				'recursiveLevel' => 'yes'
			)
		);

		$this->LIB = new TorneLIB();
	}
	function tearDown() {

	}

	function testInitLib() {
		$this->assertTrue(is_object($this->LIB));
	}
	function testTemplate() {
		$obj = @json_decode($this->LIB->template("templates/template1.txt"));
		$this->assertTrue(is_object($obj) && !empty($obj->string));
	}
	function testPluggableGraphics() {
		/** @var $graphics \TorneLIB\TorneLIB_Plugin_Graphics */
		$graphics = $this->LIB->PLUGIN->initPlugin("graphics");
		$this->assertTrue(strlen($graphics->getCaptchaString()) >= 8);
	}

	function testObjectToArrayIO() {
		$convert = $this->LIB->IO->objectsIntoArray( $this->obj );
		$this->assertTrue( is_array( $convert['a'] ) && is_array( $convert['a']['nextLevel'] ) && is_array( $convert['a']['nextLevel']['nextLevel'] ) );
	}

	function testArrayToObjectIO() {
		$convert = $this->LIB->IO->arrayObjectToStdClass($this->arr);
		$this->assertTrue(isset($convert->a) && is_object($convert->b) && isset($convert->b->c) && $convert->b->c == "d");
	}
	function testObjectToArrayLIB() {
		$convert = $this->LIB->objectsIntoArray( $this->obj );
		$this->assertTrue( is_array( $convert['a'] ) && is_array( $convert['a']['nextLevel'] ) && is_array( $convert['a']['nextLevel']['nextLevel'] ) );
	}

	function testArrayToObjectLIB() {
		$convert = $this->LIB->arrayObjectToStdClass($this->arr);
		$this->assertTrue(isset($convert->a) && is_object($convert->b) && isset($convert->b->c) && $convert->b->c == "d");
	}

}
